export interface Message {
    username:string;
    postDate:Date;
    content: string;
}